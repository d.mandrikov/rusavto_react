# about

This is an online store application implemented on React and React-Redux + TypeScript stack.

## Install dependencies

Before run the project use a command "**npm install**" to install dependencies.

### Run app

The "**yarn start**" command runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
