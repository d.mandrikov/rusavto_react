import React, { useEffect, useState } from "react";
import './Slider.css'
import {slideImages} from '../../constants'

const Slider = () => {
  const [activeSlide, setActiveSlide] = useState(0)

  useEffect(() => {
    const interval = setInterval(() => {
      setActiveSlide((current) => current === slideImages.length - 1 ? 0 : current + 1)
    }, 3500)
    return () => clearInterval(interval)
  }, [activeSlide])

  const prevSlide = activeSlide ? activeSlide - 1 : slideImages.length - 1;
  const nextSlide = activeSlide === slideImages.length - 1 ? 0 : activeSlide + 1;

  return (
    <div className='slider'>
       <div className="slider-image slider-image-prev" key={prevSlide}>
          {slideImages[prevSlide]}
       </div>
       <div className="slider-image" key={activeSlide}>
          {slideImages[activeSlide]}
       </div>
       <div className="slider-image slider-image-next" key={nextSlide}>
            {slideImages[nextSlide]}
       </div>
    </div>
  );
};

export default Slider;
