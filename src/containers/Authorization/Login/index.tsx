import React from "react";
import { useDispatch } from "react-redux";
import Backdrop from "../../../components/Backdrop";
import Button from "../../../components/Button";
import { toogleLoginWindow } from "../../../store/actions/authorizationAction";
import classes from "./Login.module.css";
import { ILogin } from "../../../constants";

const Login: React.FC<ILogin> = ({ isOpenLogin }) => {
  const dispatch = useDispatch();
  const cancelWindow = () => {
    dispatch(toogleLoginWindow());
  };
  const loginHandler = (event: any) => {
    event.preventDefault();
  };
  return (
    <>
      <div className={classes.login_div}>
        <h2>Заполните все поля для входа</h2>
        <form className={classes.Login} onSubmit={loginHandler}>
          <label htmlFor={"login"}>Логин: </label>
          <input type="text" name="login" placeholder="Введите логин" />

          <label htmlFor={"password"}>Пароль: </label>
          <input
            type="password"
            name="password"
            placeholder="Введите пароль "
          />

          <div className={classes.button_wrapper}>
            <Button type="submit" className={"submit"}>
              {"Войти"}
            </Button>
            <Button onClick={cancelWindow} className={"close"}>
              {"Отмена"}
            </Button>
          </div>
        </form>
      </div>
      {isOpenLogin ? <Backdrop cancelWindow={cancelWindow} /> : null}
    </>
  );
};
export default Login;
