import React from "react";
import { useDispatch } from "react-redux";
import Backdrop from "../../../components/Backdrop";
import Button from "../../../components/Button";
import { toogleRegistrationWindow } from "../../../store/actions/authorizationAction";
import classes from "./Registration.module.css";
import { IRegistration } from "../../../constants";

const Registration: React.FC<IRegistration> = ({ isOpenRegistration }) => {
  const dispatch = useDispatch();
  const cancelWindow = () => {
    dispatch(toogleRegistrationWindow());
  };
  const registrHandler = (event: any) => {
    event.preventDefault();
  };
  return (
    <>
      <div className={classes.Registration_div}>
        <h2>Заполните все поля для регистрации </h2>
        <form className={classes.Registration} onSubmit={registrHandler}>
          <label>Имя:</label>
          <input type="text" name="name" placeholder="Введите имя" />

          <label htmlFor={"login"}>Логин: </label>
          <input type="text" name="login" placeholder="Введите логин" />

          <label htmlFor={"password"}>Пароль: </label>
          <input
            type="password"
            name="password"
            placeholder="Введите пароль "
          />

          <div className={classes.button_wrapper}>
            <Button type="submit" className={"submit"}>
              {"Зарегистрироваться"}
            </Button>
            <Button onClick={cancelWindow} className={"close"}>
              {"Отмена"}
            </Button>
          </div>
        </form>
      </div>
      {isOpenRegistration ? <Backdrop cancelWindow={cancelWindow} /> : null}
    </>
  );
};
export default Registration;
