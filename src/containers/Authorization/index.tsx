import React from "react";
import { useSelector } from "react-redux";
import ThanksForBuy from "../../components/ThanksForBuy";
import { IAppState } from "../../constants";
import Login from "./Login";
import Registration from "./Registration";

const Authorization = () => {
  const state = useSelector((state: IAppState) => state);

  const thanks = state.isThanks.isThanks;
  const isOpenLogin = state.openAuthorization.isShowLogin;
  const isOpenRegistration = state.openAuthorization.isShowRegistration;

  return (
    <>
      {thanks ? <ThanksForBuy thanks={thanks} /> : null}
      {isOpenLogin ? <Login isOpenLogin={isOpenLogin} /> : null}
      {isOpenRegistration ? (
        <Registration isOpenRegistration={isOpenRegistration} />
      ) : null}
    </>
  );
};
export default Authorization;
