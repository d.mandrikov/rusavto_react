import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import NavMenu from "../../components/NavMenu";
import Cars from "./Cars";
import "./Content.css";
import MainPage from "./MainPage";
import Trucks from "./Trucks";

function Content() {
  return (
    <div className="Content">
      <NavMenu className={'NavMenu_content'}/>
      <Switch>
        <Route path="/" exact component={MainPage} />
        <Route path="/cars" component={Cars} />
        <Route path="/trucks" component={Trucks} />
        <Redirect to={"/"} />
      </Switch>
    </div>
  );
}

export default Content;
