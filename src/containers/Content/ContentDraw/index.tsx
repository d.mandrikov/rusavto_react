import React from "react";
import Item from "../../../components/Item";
import "./ContentDraw.css";
import { useSelector } from "react-redux";
import { IAppState } from "../../../constants";

const ContentDraw = () => {
  const cards = useSelector((state: IAppState) => state.autos.autos);

  return (
    <div className="main-content">
      {cards.map((card, index: number) => (
        <Item key={index + card.aboutText} card={card} />
      ))}
    </div>
  );
};

export default ContentDraw;