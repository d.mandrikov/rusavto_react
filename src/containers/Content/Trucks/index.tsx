import React from 'react';
import ContentDraw from '../ContentDraw';
import './Trucks.css';

const Trucks = () => {
  return (
    <div className="Trucks">
      <ContentDraw />
    </div>
  );
}

export default Trucks;
