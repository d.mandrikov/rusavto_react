import React from 'react';
import ContentDraw from '../ContentDraw';
import './Cars.css';

const Cars = () => {
  return (
    <div className="Cars">
      <ContentDraw />
    </div>
  );
}

export default Cars;
