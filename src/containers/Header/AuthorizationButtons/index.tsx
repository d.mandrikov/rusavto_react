import React from 'react';
import { useDispatch } from 'react-redux';
import Button from '../../../components/Button';
import { toogleLoginWindow, toogleRegistrationWindow } from '../../../store/actions/authorizationAction';
import './AuthorizationButtons.css';

const AuthorizationButtons = () => {
  const dispatch = useDispatch()

  const loginHandler = () => {
    dispatch(toogleLoginWindow())
  }
  const registrationHandler = () => {
    dispatch(toogleRegistrationWindow())
  }
  return (
    <div className="AuthorizationButtons">
      <Button  onClick={loginHandler}>{'Войти'}</Button>
      <Button  onClick={registrationHandler}>{'Зарегистрироваться'}</Button>
    </div>
  );
}

export default AuthorizationButtons;
