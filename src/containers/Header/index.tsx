import React from "react";
import "./Header.css";
import Logo from "../../components/Logo";
import NavMenu from "../../components/NavMenu";
import AuthorizationButtons from "./AuthorizationButtons";

const Header = () => {
  return (
    <header className="header">
      <Logo />
      <NavMenu className={"NavMenu_header"} />
      <AuthorizationButtons />
    </header>
  );
};
export default Header;