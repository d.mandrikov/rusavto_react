import React from 'react'
import classes from '../Footer.module.css'
import { IFooter } from '../../../constants';

const FooterLink: React.FC<IFooter> = ({link}) => {
    const openLink = () => {
        window.open(link.link)
    }
    return (
        <>
            <p className={classes[link.class]} onClick={openLink}>{link.nameOfLink}</p>
        </>
    )
}
export default FooterLink;