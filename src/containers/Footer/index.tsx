import React from 'react'
import classes from './Footer.module.css'
import {navFooterList} from '../../constants'
import FooterLink from './footerLink'

const Footer = () => {
    return (
        <footer className={classes.footer}>
             {navFooterList.map((link, index: number) => (
                 <FooterLink key={index + link.nameOfLink} link={link}/>
             ))}
             <p className={classes.made}>Made by Dmitry Mandrikov</p>
        </footer>
    )
}
export default Footer;