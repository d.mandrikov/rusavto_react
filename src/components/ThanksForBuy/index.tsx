import React from "react";
import { useDispatch } from "react-redux";
import { thanksOnClick } from "../../store/actions/thanksAction";
import Backdrop from "../Backdrop";
import classes from "./ThanksForBuy.module.css";
import { IThanksForBuy } from "../../constants";

const ThanksForBuy: React.FC<IThanksForBuy> = ({ thanks }) => {
  const dispatch = useDispatch();
  const cancelWindow = () => {
    dispatch(thanksOnClick());
  };
  return (
    <>
      <div className={classes.ThanksForBuy}>
        <h1>Спасибо за покупку</h1>
      </div>
      {thanks ? <Backdrop cancelWindow={cancelWindow} /> : null}
    </>
  );
};
export default ThanksForBuy;
