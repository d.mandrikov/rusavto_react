import classes from './Backdrop.module.css'
import { IBackdrop } from '../../constants'
const Backdrop: React.FC<IBackdrop> = ({ cancelWindow }) => {
    return <div className={classes.Backdrop} onClick={cancelWindow}/>
}
export default Backdrop;