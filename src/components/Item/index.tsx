import React from "react";
import { useDispatch } from "react-redux";
import { IContentDraw } from "../../constants";
import { thanksOnClick } from "../../store/actions/thanksAction";
import Button from "../Button";

const Item: React.FC<IContentDraw> = ({ card }) => {
  const dispatch = useDispatch();
  const thanksForBuyHandler = () => {
    dispatch(thanksOnClick());
  };
  return (
    <div className="card">
      <div className="image">
        <img src={card.src} className="card-img-top" alt="..." />
      </div>
      <div className="card-body">
        <h5 className="card-title">{card.titleText}</h5>
        <p className="card-text">{card.aboutText}</p>
        <p className="card-rating">{card.rating}</p>
        <p className="card-price">{card.price}</p>
        <Button onClick={thanksForBuyHandler}>
          {"КУПИТЬ"}
        </Button>
      </div>
    </div>
  );
};
export default Item;