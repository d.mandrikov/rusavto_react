import React from "react";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  openCars,
  openAll,
  openTrucks,
} from "../../store/actions/filterAction";
import {ItemType, NavMenuProps} from '../../constants'
import Button from "../Button";

const NavMenu = ({className}: NavMenuProps) => {
  const autos = useSelector((state: any) => state);
  const cards: ItemType = autos.autos.autos;

  const dispatch = useDispatch();

  const openAllAuto = () => {
    dispatch(openAll(cards.type));
  };

  const openCar = () => {
    dispatch(openCars(cards.type));
  };

  const openTruck = () => {
    dispatch(openTrucks(cards.type));
  };

  return (
    <nav className={className}>
      <Button onClick={openAllAuto}><NavLink to="/" exact>
        Главная
      </NavLink></Button>
      <Button onClick={openCar}><NavLink to="/cars">
        Легковые автомобили
      </NavLink></Button>
      
      <Button onClick={openTruck}><NavLink to="/trucks">
        Грузовые автомобили
      </NavLink></Button>
      
    </nav>
  );
};
export default NavMenu;
