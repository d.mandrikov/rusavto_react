import React from 'react'
import './Logo.css'

const Logo = () => {
    return (
        <div className='logo'>Rusavto</div>
    )
}
export default Logo