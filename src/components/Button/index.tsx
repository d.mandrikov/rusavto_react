import classes from './Button.module.css';
import {IButton} from '../../constants'

const Button: React.FC<IButton> = ({onClick, children, className}) => {
  const cls: string[] = [classes.Button]
  if (!!className) {
    cls.push(classes[className])
  }  
  return (
    <button 
        className={cls.join(' ')}
        onClick={onClick}
        >{children}</button>
  );
}
export default Button;