import lada1 from "../assets/images/cars/lada1.jpg";
import lada99 from "../assets/images/cars/99.jpg";
import lada7 from "../assets/images/cars/lada7.jpg";
import priora from "../assets/images/cars/priora7.jpg";
import uaz from "../assets/images/cars/uaz-pickup.jpg";
import xray from "../assets/images/cars/XRAY-cross.jpg";
import avtobus from "../assets/images/trucks/avtobus.jpg";
import kamaz1 from "../assets/images/trucks/kamaz1.jpg";
import kamaz2 from "../assets/images/trucks/kamaz2.jpg";
import sobol_bis from "../assets/images/trucks/sobol_bis.jpg";
import sobol from "../assets/images/trucks/sobol1.jpg";
import zil from "../assets/images/trucks/zil.jpg";
import { ItemType } from "../constants";


const itemsListAuto: ItemType[] = [
  {
    src: avtobus,
    titleText: "ГАЗ",
    aboutText: "Автомобиль для транспортировки людей.",
    rating: 2,
    price: "2 000 000р",
    type: "trucks",
  },
  {
    src: kamaz1,
    titleText: "КАМАЗ",
    aboutText: "Грузовик зарекомендовавший себя своей надежностью.",
    rating: 3.5,
    price: "250 000р",
    type: "trucks",
  },
  {
    src: kamaz2,
    titleText: "КАМАЗ",
    aboutText: "Грузовик зарекомендовавший себя своей надежностью.",
    rating: 4,
    price: "200 000р",
    type: "trucks",
  },
  {
    src: priora,
    titleText: "Lada priora",
    aboutText: "Классический седан.",
    rating: 4,
    price: "150 000р",
    type: "cars",
  },
  {
    src: uaz,
    titleText: "УАЗ",
    aboutText: "Отличный внедорожник.",
    rating: 2,
    price: "400 000р",
    type: "cars",
  },
  {
    src: lada99,
    titleText: "Lada 99",
    aboutText: "Лада кабриолет.",
    rating: 4.5,
    price: "400 000р",
    type: "cars",
  },
  {
    src: lada1,
    titleText: "Лада копейка",
    aboutText: "Классический седан, советского производства.",
    rating: 2,
    price: "150 000р",
    type: "cars",
  },
  {
    src: sobol,
    titleText: "Соболь",
    aboutText: "Маршрутное такси.",
    rating: 3.5,
    price: "600 000р",
    type: "trucks",
  },
  {
    src: lada7,
    titleText: "Лада 7",
    aboutText: "Классический седан.",
    rating: 3,
    price: "200 000р",
    type: "cars",
  },
  {
    src: sobol_bis,
    titleText: "Соболь бизнес",
    aboutText:
      "Отличный автомобиль для транспортировки среднегабаритных грузов.",
    rating: 5,
    price: "1 500 000р",
    type: "trucks",
  },
  {
    src: zil,
    titleText: "ЗИЛ",
    aboutText: "Грузовик с открытым кузовом.",
    rating: 3.5,
    price: "400 000р",
    type: "trucks",
  },
  {
    src: xray,
    titleText: "Lada Xray",
    aboutText: "Новое поколение лады.",
    rating: 4.5,
    price: "1 200 000р",
    type: "cars",
  },
];
export default itemsListAuto;
