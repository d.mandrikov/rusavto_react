import slide1 from "../assets/images/slider/slide1.jpg";
import slide2 from "../assets/images/slider/slide2.jpg";
import slide3 from "../assets/images/slider/slide3.jpg";

export const slideImages = [
  <img key={slide1} src={slide1} alt="first" />,
  <img key={slide2} src={slide2} alt="second" />,
  <img key={slide3} src={slide3} alt="third" />,
];

export interface Ilink {
  nameOfLink: string;
  link: string;
  class: string;
}
export const navFooterList: Ilink[] = [
  {
    nameOfLink: "О машинах",
    link: "https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D1%88%D0%B8%D0%BD%D0%B0",
    class: "footer_link1",
  },
  {
    nameOfLink: "Самые безопасные автомобили",
    link:
      "https://rg.ru/2020/10/16/samye-bezopasnye-avtomobili-do-1-mln-rublej.html",
    class: "footer_link2",
  },
  {
    nameOfLink: "Планы российского автопрома",
    link:
      "https://rg.ru/2019/01/08/chem-poraduet-rossijskij-avtoprom-v-blizhajshie-gody.html",
    class: "footer_link3",
  },
  {
    nameOfLink: "Форум",
    link: "http://www.autoforum.pro",
    class: "footer_link4",
  },
];

export interface IAppState {
  autos: IInitialStateFilter;
  isThanks: IInitialStateThanks;
  openAuthorization: IInitialStateAuthorization;
}

export interface ItemType {
  src: string;
  titleText: string;
  aboutText: string;
  rating: number;
  price: string;
  type: string;
}

export interface IInitialStateAuthorization {
  isShowLogin: boolean;
  isShowRegistration: boolean;
}

export interface IAuthorizationAction {
  type: string;
}
export interface IThanksAction {
  type: string;
}

export type IInitialStateFilter = {
  autos: ItemType[];
};
export interface IFilterAction {
  type: string;
  payload: string;
}
export interface IInitialStateThanks {
  isThanks: boolean;
}
export interface NavMenuProps {
  className: string;
}

export interface IFooter {
  link: Ilink;
}
export interface IContentDraw {
  card: ItemType;
}
export interface ILogin {
  isOpenLogin: boolean;
}
export interface IThanksForBuy {
  thanks: boolean;
}
export interface IRegistration {
  isOpenRegistration: boolean;
}
export interface IBackdrop {
  cancelWindow: VoidFunction;
}
export interface IButton {
  className?: string;
  onClick?: VoidFunction;
  type?: string;
}