import React from "react";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import Slider from "./containers/Slider";
import Content from "./containers/Content";
import Footer from "./containers/Footer";
import Header from "./containers/Header";
import Authorization from "./containers/Authorization";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Slider />
      <Content />
      <Footer />
      <Authorization />
    </BrowserRouter>
  );
}
export default App;
