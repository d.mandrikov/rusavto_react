import { IThanksAction } from './../../constants';
export const THANKS_FOR_BUY: string = 'THANKS_FOR_BUY'

export const thanksOnClick = (): IThanksAction => {
    return {
        type: THANKS_FOR_BUY
    }
}