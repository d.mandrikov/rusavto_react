import { IFilterAction } from "../../constants"

export const OPEN_ALL: string = 'OPEN_ALL'
export const OPEN_CARS: string = 'OPEN_CARS'
export const OPEN_TRUCKS: string = 'OPEN_TRUCKS'


export const openAll = (cartType: string): IFilterAction => {
    return {
        type: OPEN_ALL,
        payload: cartType
    }
}

export const openCars = (cartType: string): IFilterAction => {
    return {
        type: OPEN_CARS,
        payload: cartType
    }
}

export const openTrucks = (cartType: string): IFilterAction => {
    return {
        type: OPEN_TRUCKS,
        payload: cartType
    }
}