import { IAuthorizationAction } from './../../constants';

export const LOGIN: string = 'LOGIN'
export const REGISTRATION: string = 'REGISTRATION'

export const toogleLoginWindow = (): IAuthorizationAction => {
    return {
        type: LOGIN
    }
}
export const toogleRegistrationWindow = (): IAuthorizationAction => {
    return {
        type: REGISTRATION
    }
}