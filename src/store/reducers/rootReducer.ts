import { combineReducers } from "redux";
import authorizationReducer from "./authorizationReducer";
import filterReduser from "./filterReducer";
import thanksReducer from './thanksReducer'

 const rootReducer = combineReducers({
  autos: filterReduser,
  isThanks: thanksReducer,
  openAuthorization: authorizationReducer,
});
export default rootReducer

export type RootState = ReturnType<typeof rootReducer>