import { IAuthorizationAction, IInitialStateAuthorization } from "../../constants";
import { LOGIN, REGISTRATION } from "../actions/authorizationAction";



const initialState: IInitialStateAuthorization = {
  isShowLogin: false,
  isShowRegistration: false,
};

const authorizationReducer = (state = initialState, action: IAuthorizationAction) => {
  const { isShowLogin, isShowRegistration } = state;
  switch (action.type) {
    case LOGIN:
      return {
          ...state,
          isShowLogin: !isShowLogin
      }
      case REGISTRATION: 
      return {
        ...state,
        isShowRegistration: !isShowRegistration
      }
    default:
      return state;
  }
};
export default authorizationReducer;