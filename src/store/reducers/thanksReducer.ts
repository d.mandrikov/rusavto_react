import { IInitialStateThanks, IThanksAction } from "../../constants";
import { THANKS_FOR_BUY } from "../actions/thanksAction";

const initialState: IInitialStateThanks = {
  isThanks: false,
};

const thanksReducer = (state = initialState, action: IThanksAction) => {
  const { isThanks } = state;
  switch (action.type) {
    case THANKS_FOR_BUY:
      return {
          ...state,
          isThanks: !isThanks
      }
    default:
      return state;
  }
};
export default thanksReducer;
