import data  from './../../data/data';
import {OPEN_ALL, OPEN_CARS, OPEN_TRUCKS} from '../actions/filterAction'
import { IFilterAction, IInitialStateFilter, ItemType } from '../../constants';



const initialState: IInitialStateFilter = {
    autos: data
}

const filterReducer = (state = initialState, action: IFilterAction) => {
    switch (action.type) {
        case OPEN_ALL:
            return {
                ...state, 
                autos: data
            }

        case OPEN_CARS:
            const cars: ItemType[] = data.filter(auto => auto.type === 'cars')
            return {
                ...state,
                autos: cars
            }
            
        case OPEN_TRUCKS:
            const trucs: ItemType[] = data.filter(auto => auto.type === 'trucks')
            return {
                
                ...state,
                autos: trucs
            }
         default: 
            return state
    }
}
export default filterReducer;